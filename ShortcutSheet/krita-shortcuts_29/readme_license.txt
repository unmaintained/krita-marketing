
# ---------------:::: Krita Shortcuts Sheet ::::-----------------------
#  Version: v2.8
#  Author:IvanYossi / http://colorathis.wordpress.com ghevan@gmail.com
#  License: Creative commons
#    Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)
# 
#  More info:
#    http://creativecommons.org/licenses/by-sa/3.0/
#
# -----------------------------------------------------------


Default Sans font:
Bitstream Vera Sans

Font explanations
Oxygen (Bundled)